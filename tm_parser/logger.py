import logging
import sys

logging.basicConfig(stream=sys.stderr)

logger = logging.getLogger(__name__)
